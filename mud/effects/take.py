# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import TakeEvent, TakeWithEvent

class TakeEffect(Effect2):
    EVENT = TakeEvent

class TakeWithEffect(Effect3):
    EVENT = TakeWithEvent
