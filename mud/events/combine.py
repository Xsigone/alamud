from .event import Event3

class CombineWithEvent(Event3):
    NAME = "combine-with"

    def perform(self):
        self.inform("combine-with")
